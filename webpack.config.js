const path = require("path");
const fs = require('fs');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const glob = require("glob");

function generateHtmlPlugins (templateDir) {
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));

    return templateFiles.map(item => {
        const parts = item.split('.');
        const name = parts[0];
        const extension = parts[1];
        return new HtmlWebpackPlugin({
            filename: `${templateDir}/${name}.html`,
            template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
        })
    });

}
const htmlPlugins = generateHtmlPlugins('./app/pages');

module.exports = {
    entry: {
        app: "./app/main.js"
    },

    output: {
        path: path.join(__dirname, "/dist"),
        filename: "./app/scripts/[name].js",
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        'scss': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader'
                        ],
                        'sass': [
                            'vue-style-loader',
                            'css-loader',
                            'sass-loader?indentedSyntax'
                        ],
                        js: 'babel-loader!eslint-loader'
                    }
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader",
                    "import-glob"
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: './app/common/images/'
                        },
                    },
                ],
            },
            {
                test: /.(ttf|otf|eot|svg|woff|woff2)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: "./app/common/fonts/"
                    }
                }
            },
            {
                test: /\.svg$/,
                loader: 'svg-inline-loader?classPrefix'
            }
        ]
    },

    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['*', '.js', '.vue', '.json']
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        public: 'localhost:8080',
        before:(app) => {
            app.post('*', function(req, res, next) {
                res.json({success: true})
            });
        }
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: './app/pages/main.html'
        }),

        new MiniCssExtractPlugin({
            filename: "./app/style/main.css"
        }),

        new VueLoaderPlugin(),

        new CopyWebpackPlugin([
            {
                from:'./app/common/images',
                to:'./app/common/images'
            },
            {
                from:'./app/common/fonts',
                to:'./app/common/fonts'
            },
            {
                from:'./app/common/icons',
                to:'./app/common/icons'
            },
            {
                from:'./app/json',
                to:'./app/json'
            },
        ]),

    ]
        .concat(htmlPlugins)
};